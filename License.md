このソフトウェアは以下のデータを利用している

# Food Truckのデータ
* data/Mobile_Food_Facility_Permit.csv
    * https://data.sfgov.org/Economy-and-Community/Mobile-Food-Facility-Permit/rqzj-sfatからCSVをダウンロード

# San Francisco市の白地図
* data/san-francisco.geojson.json
    * https://github.com/codeforamerica/click_that_hood/blob/master/public/data/san-francisco.geojson をダウンロード
* data/SanFrancisco.Neighborhoods.json
    * https://gist.github.com/cdolek/d08cac2fa3f6338d84ea をダウンロード
    * Food Truckのデータの緯度経度とずれがあるので使用しなかった