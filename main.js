/*
 * Food Truck Navigator
 * by K. Tone
 * on 2018-08-26
 *
 **********************************
 * main.js
 * (1)地図と店のアイコンの描画・ズーム・ドラッグ
 * (2)店のListの表示
 * (3)Food Itemによるフィルタリング
 */

/* global d3 */

import * as G from "./initialize.js"; // import global const and var

// 検索用フィールドへのonchangeイベント登録
window.onload = function() {
//  console.log("LOADED");
    document.getElementById("findFood").onchange = function() { updateTrucks(this.value) };
};


//店のプロット領域オブジェクト定義
var icons = {};

//Mapの射影情報
var aProjection = d3.geoMercator()
    .center([G.centerLongitude, G.centerLatitude])
    .translate([G.width / 2, G.height / 2])
    .scale(G.scale);


//店ファイルの持つ経度緯度の座標変換関数
function getCoordinate(d, L) { // L=0 => X座標, L=1 => Y座標を返す
    var coordinate = aProjection([d.Longitude, d.Latitude]);
    return coordinate[L];
}

var geoPath = d3.geoPath().projection(aProjection);

var svg = d3.select("#map")
    .style("border", G.mapBorder)
    .attr("width", G.width) // 幅・高さを指定する
    .attr("height", G.height);

//ズーム対象とするレイヤーを生成	
var zoomLayer = svg.append("g");



//tooltip用div要素追加
var tooltip = d3.select("body").append("div").attr("class", "tooltip");


// bodyにtable領域を追加
var table = d3.select("body")
    .append("table")
    .style("border", G.tableBorder); //tableタグ追加
var thead = table.append("thead"); //theadタグ追加
var tbody = table.append("tbody"); //tbodyタグ追加

//地図描画
d3.json(G.geoJsonFile).then(function(data) {

    var map = zoomLayer.selectAll("path").data(data.features)
        .enter()
        .append("path")
        .attr("d", geoPath)
        .style("stroke", G.mapStrokeColor)
        .style("stroke-width", G.mapStrokeWidth)
        .style("fill", G.mapFillColor);

});

//Truckのアイコン描画

function updateTrucks(item) { // 新しい条件で描画を更新
    icons.remove();
    table.remove();
    table = d3.select("body")
        .append("table")
        .style("border", G.mapBorder); //tableタグ追加
    thead = table.append("thead"); //theadタグ追加
    tbody = table.append("tbody"); //tbodyタグ追加
    var newItem;
    if (item.length <= 0) {
        newItem = " ";
    }
    else {
        newItem = item;
    }
    drawTrucks(newItem);
}

// itemがFoodItemsカラムに含まれるかをcaseを無視して判定。itemがスペースならばすべてを選択。
function selectTrucks(d, item) {
    if (item == " ") {
        return true;
    }
    else {
        return d.FoodItems.toLowerCase().indexOf(item.toLowerCase()) > -1;
    }
}


// カラムの番号が特定の番号より大きいものは表示しない
function selectColumns(d, i) {
    if (i < G.maxColumnNum) {
        return true;
    }
    else {
        return false;
    }
}

function drawTrucks(item) {
    d3.csv(G.csvFile).then(function(data) {
        icons = zoomLayer.selectAll("icons")
            .data(data)
            .enter()
            .filter(function(d) { return selectTrucks(d, item) })
            .append("g");

        //円の描画
        icons.append("circle")
            .attr("cx", function(d) { return getCoordinate(d, 0) })
            .attr("cy", function(d) { return getCoordinate(d, 1) })
            .attr("r", G.circleSize)
            .attr("opacity", G.circleOpacity) // 透明度
            .attr("stroke", G.circleStrokeColor) // 枠線色
            .attr("fill", G.circleFillColor) // 塗りつぶし色
            .on("mouseover", function(d) { drawTooltip(d) })
            .on("mouseout", function(d) {
                tooltip.style("visibility", "hidden");
            });

        //店名の表示
        icons.append("text")
            .text(function(d) { return d.Applicant; })
            .attr("x", function(d) { return getCoordinate(d, 0) })
            .attr("y", function(d) { return getCoordinate(d, 1) })
            .attr("fill", G.circleTextColor) //Textの色
            .attr("font-size", G.circleFontSize);

        function drawTooltip(d) {
            tooltip.style("visibility", "visible")
                .style("top", (d3.event.pageY - 20) + "px")
                .style("left", (d3.event.pageX + 10) + "px")
                .html(d.Applicant + "<br>" + d.Address + "<br>" + d.FoodItems.substr(0, 50) + "<br>" + d.dayshours.substr(0, 50));
        }

    });

    d3.csv(G.csvFile).then(function(data) {
        var headerKyes = d3.map(data[0]).keys(); //ヘッダー用にkeyを取得
        var itemCount = 0;


        // Table Body
        tbody.selectAll("tr")
            .data(data)
            .enter()
            .filter(function(d) {
                if (selectTrucks(d, item)) {
                    itemCount++;
                    return true;
                }
                else {
                    return false;
                }
            })
            .append("tr") //trタグ追加
            .selectAll("td")
            .data(function(row) {
                return d3.entries(row); //rowオブジェクトを配列へ変換
            })
            .enter()
            .filter(function(d, i) { return selectColumns(d, i) })
            .append("td") //tdタグ追加
            .style("border", G.tableBorder)
            .text(function(d) { return d.value });

        // Table Headerを最後に編集

        //該当の店がなければメッセージ表示あれば件数表示
        if (itemCount == 0) {
            thead.append('tr') //trタグ追加
                .selectAll('th')
                .data(" ")
                .enter()
                .append('th') //thタグ追加
                .text(function(d) { return "No trucks found" });
        }
        else {
            thead.append('tr') //trタグ追加
                .selectAll('th')
                .data(" ")
                .enter()
                .append('th') //thタグ追加
                .text(function(d) { return itemCount + " truck(s) found" });

            thead.append('tr') //trタグ追加
                .selectAll('th')
                .data(headerKyes)
                .enter()
                .filter(function(d, i) { return selectColumns(d, i) })
                .append('th') //thタグ追加
                .text(function(key) { return key });
        }

    });
}


// 初回は全件表示。地図の後で表示されるように2秒待つ
setTimeout(function() { drawTrucks(" ") }, 2000);


//ズーム時の処理を設定
var zoomed = function() {
    zoomLayer.attr("transform", d3.event.transform);
};

//ズームイベントをsvg要素に束縛
svg.call(d3.zoom()
    .scaleExtent([1, G.maxZoomSize]) //拡大縮小の限度を設定
    .on("zoom", zoomed));
// なぜかズーム処理を実装するとパンも動作する
