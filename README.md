# Food-Trucks

## 課題
https://github.com/uber/coding-challenge-tools/blob/master/coding_challenge.md
から

Food Trucks

Create a service that tells the user what types of food trucks might be found near a specific location on a map.

を選択

#### 選択理由
* Departure Timesは時刻表APIのテスト・学習に工数がかかりそう
* SF Moviesはロケ地の住所記述があいまいでその緯度経度変換に工数が必要、かつ緯度経度が求められない場合の例外処理に悩む
    * また行のユニークなIDがないのも不便
* Email Serviceはバックエンドでのfailoverが難しそう

であるのに対して

* Food Trucksはデータに最初から緯度経度があり、かつ行のユニークなIDがあるので扱いやすい

ということ

## ソリューション
* 店(Food Truck)の場所のアイコンをSFの地図上にプロットし、同時に店の一覧表も表示する
* 食べ物(Food Items)の検索用のキーワードを含む店のみを表示する

## 機能
* 店(Food Truck)の場所を円で表示しSFの地図上に店名と共にプロットする
    * Status="Approved"の店および緯度経度がゼロでない店のみを抽出したCSVファイルを用いる
* 地図の初期表示はSF全体とするが、マウス操作で店のアイコンと共に移動・ズーム可能
* 食べ物をキーワードで検索し該当の店のみを表示
    * 入力されたスペースも含む任意のキーワードが店の食べ物に含まれる店のみを表示
    * 該当の店が存在しない場合は地図や一覧表には何も表示されない
* 店のアイコンのツールチップで店名、住所、営業時間、食べ物を表示
* 地図の下に店の件数と一覧表を表示
 

## 実装方針
* フロントエンド中心で実装
    * バックエンドの地図データ・店のデータが合計で500KB以下かつ店のデータも更新頻度が週次なのでDBサーバーは不要
    * 複雑なルーティングやビジネスルールも存在しないのでWebアプリサーバは不要
    * 利用しているデータはオープンソースまたはオープンデータであり参照についての保護は不要なのでファイルとしてサイトに設置する
* 静的サイトをサイトホスティングサービスの提供するGlobal CDN(今回はNetlifyを使用)にデプロイする
    * DBサーバーやWebアプリサーバーが存在しないのでこれらに伴う脆弱性が回避できる
    * CDNの持つ負荷分散やスケーラビリティの機能を利用できる

## 実装内容
* d3.jsを用いた静的サイトをNetlifyに公開
    * Webpackなどは用いずhtmlファイルからJSを直接参照する
    * 公開URLはhttps://compassionate-montalcini-e3967b.netlify.com/
    * Bitbucketと同期しpushすれば即座に更新されるように設定している
* SFの地図はGeoJsonを用いて描画
    * データは https://github.com/codeforamerica/click_that_hood/blob/master/public/data/san-francisco.geojson を用いる
* 店のデータはhttps://data.sfgov.org/Economy-and-Community/Mobile-Food-Facility-Permit/rqzj-sfatからCSVをダウンロード
    * Status="Approved"のみ抽出し必要なカラムのみを持つCSVをd3.jsに取り込む

## ToDo
* Reactで実装し複雑な機能追加やPWAのビルドを容易にする
    * 特に以下の描画順序の制御を実装すると処理が複雑になるのでReactなどが必要
* 初期表示で店のアイコンを必ず地図の上に描画する
    * SVGでは図形のレイヤーの概念がなく後から描画したものが上になる
    * 初期表示では地図の描画が遅くなり、アイコンが下になるのでTooltipが無効になる
        *  やむを得ず初期表示はアイコン表示を2秒待たせている
    * Food検索時は地図は再描画されないのでアイコンが上になる
* CSVデータの自動更新と自動デプロイ
* 食べ物検索の機能追加
    * AND/OR条件
    * ワイルドカード
* 店のタイプを分類しそのカテゴリ毎にアイコンの色や形を変える
* PWAとしてビルドしモバイル電話のGPSに連動して初期表示の位置を決定する

## 補足

### 職務経歴書
* [リンク](https://drive.google.com/file/d/1W7u2uIurWhNtxQfa0Aw9RujDyGF--D8k/view?usp=sharing)

### 現在稼働しているコード
「particularly proud of」ではないが一応公開運用中

* Gmailから投稿する掲示板システム
    * https://github.com/kunichan2013/MailBlog
* フォルダを巡回しその中の動画ファイルをMP4とFLV形式に変換するジョブ
    * https://github.com/kunichan2013/VideoConverter


### 技術資料
* d3.js チュートリアル
    * http://deep-blend.com/jp/2014/05/d3-js-basic1-make-template/ 
* Food Truck メタデータ
    * https://data.sfgov.org/api/views/rqzj-sfat/files/8g2f5RV4PEk0_b24iJEtgEet9gnh_eA27GlqoOjjK4k?download=true&filename=DPW_DataDictionary_Mobile-Food-Facility-Permit.pdf
* ツールチップ表示方法
    * http://blog.aagata.ciao.jp/?eid=125
* 地図の描画
    * https://qiita.com/sand/items/422d4fab77ea8f69dfdf
* ズームと移動
    * http://bl.ocks.org/shimizu/bf7f84066d10c71bd61d716500b26a31/780a83bcf2936ec9195021d0d734443d4f05ff5c
* マウスイベント
    * http://koyamatch.com/d3js/d3js11.html 
* CSVから表の生成
    * https://qiita.com/_shimizu/items/0bb1ed4199b500670e69