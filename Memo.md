# Food-Trucks

## 課題
https://github.com/uber/coding-challenge-tools/blob/master/coding_challenge.md
から

Food Trucks

Create a service that tells the user what types of food trucks might be found near a specific location on a map.

を選択

#### 選択理由
* Departure Timesは時刻表APIのテスト・学習に工数がかかりそう
* SF Moviesはロケ地の住所記述があいまいでその緯度経度変換に工数が必要、かつ緯度経度が求められない場合の例外処理に悩む
    * また行のユニークなIDがないのも不便
* Email Serviceはバックエンドでのfailoverが難しそう

であるのに対して

* Food Trucksはデータに最初から緯度経度があり、かつ行のユニークなIDがあるので扱いやすい

ということ

## 機能
* 店(Food Truck)の場所をSFの地図上にプロットする
    * Status="Approved"の店および緯度経度がゼロでない店のみを選択
* 地図の初期表示はSF全体とするが、マウス操作で移動・ズーム可能
* Fooditemsで検索し該当の店のみを表示
* 店のアイコンのツールチップで店名、住所、営業時間、Fooditemsを表示
* 地図の下に店の件数と一覧表も表示
 

## 実装
* d3.jsを用いた静的ページをNetlifyに公開
* SFの地図はGeoJsonを用いて描画
    * データは https://github.com/codeforamerica/click_that_hood/blob/master/public/data/san-francisco.geojson を用いる
* 店のデータはhttps://data.sfgov.org/Economy-and-Community/Mobile-Food-Facility-Permit/rqzj-sfatからCSVをダウンロード
    * Status="Approved"のみ抽出し必要なカラムのみを持つCSVをd3.jsに取り込む

## ToDo
* 初期表示で店のアイコンを必ず地図の上に描画する
    * SVGでは図形のレイヤーの概念がなく後から描画したものが上になる
    * 初期表示では地図の描画が遅くなり、アイコンが下になるのでTooltipが無効になる
        *  やむを得ず初期表示はアイコン表示を2秒待たせている
    * Food検索時は地図は再描画されないのでアイコンが上になる
* CSVデータの更新と自動デプロイ

## 参考
* d3.js チュートリアル
    * http://deep-blend.com/jp/2014/05/d3-js-basic1-make-template/ 
* Food Truck メタデータ
    * https://data.sfgov.org/api/views/rqzj-sfat/files/8g2f5RV4PEk0_b24iJEtgEet9gnh_eA27GlqoOjjK4k?download=true&filename=DPW_DataDictionary_Mobile-Food-Facility-Permit.pdf
* ツールチップ表示方法
    * http://blog.aagata.ciao.jp/?eid=125
* 地図の描画
    * https://qiita.com/sand/items/422d4fab77ea8f69dfdf
* ズームと移動
    * http://bl.ocks.org/shimizu/bf7f84066d10c71bd61d716500b26a31/780a83bcf2936ec9195021d0d734443d4f05ff5c
* マウスイベント
    * http://koyamatch.com/d3js/d3js11.html 
* CSVから表の生成
    * https://qiita.com/_shimizu/items/0bb1ed4199b500670e69 