/*
 * Food Truck Navigator
 * by K. Tone
 * on 2018-08-26
 *
 **********************************
 * initiallize.js
 * 各種グローバル定数の定義
 */

//************ 定数 **************

/* global d3 */

//Map領域の幅、高さ、スケール
export const width = 900,
    height = 600,
    scale = 260000;

//Map表示の中心の経度緯度    
export const centerLongitude = -122.430,
    centerLatitude = 37.76;

export const mapFillColor = "rgba(100, 100, 100, 0.2)", //Mapの塗りつぶし色
    //店のプロットを隠さないように半透明に設定
    mapBorder = "#ff0000 solid 1px", //Map領域の枠線のスタイル
    mapStrokeColor = "#000000", //Map内の線の色
    mapStrokeWidth = 0.2; //Map内の線の太さ

//店のアイコン
export const circleSize = 4; //円の半径
export const circleOpacity = 0.3; //円の透明度
export const circleStrokeColor = "green"; //円の枠色
export const circleFillColor = "green"; //円の塗りつぶし色
export const circleTextColor = "blue"; //円に表示するText色
export const circleFontSize = 3; //  そのフォントサイズ

//Zoom処理
export const maxZoomSize = 15; //Zoomの最大倍率


//list表示
export const tableBorder = "#000000 solid 1px";
export const maxColumnNum = 5; //カラム番号の最大値

//Map描画データファイル名
export const geoJsonFile = "data/san-francisco.geojson.json"; //このデータの方が緯度経度が正しい

//店(Food Truck)のデータ
export const csvFile = "data/Food_Trucks.csv";



//****************************************************
